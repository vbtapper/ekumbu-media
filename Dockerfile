FROM node

WORKDIR /usr/src/app

RUN mkdir /mediadata
RUN npm install && npm install -g nodemon
COPY . .

EXPOSE 3009

CMD ["node", "server.js"]
