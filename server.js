#!/usr/local/bin/node
//'use strict';

const Hapi              = require('hapi');
const fs                = require('fs');
const Joi               = require('joi');
const Boom              = require('boom');
const Inert             = require('inert');
const Path              = require('path');
const AuthBearer        = require('hapi-auth-bearer-token');
const randtoken         = require('rand-token').generator();

const config            = require('./config.js');


const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'documents')
            }
        }
    }
});

server.connection({ port: 3009 });

//registering

server.register(AuthBearer, (err) => {
    if (err) {
        throw err;
    }

    server.auth.strategy('manager', 'bearer-access-token', {
        allowQueryToken: true,              // optional, true by default
        allowMultipleHeaders: true,        // optional, false by default
        accessTokenName: 'access_token',    // optional, 'access_token' by default
        validateFunc: function (token, callback) {          
            var request = this;  
            if(token === config.Admin.accessKey) {
                return callback(null, true, config.Admin);
            }
            else {
                return callback(null, false);
            }
        }
    });

    server.route([
        {
            /**
             * Create an account for ekumbu customer
             * params
             *      - file : the name of the file to be stored
             *      curl -F file=@tapr2.pdf localhost:3008/post -H "Authorization:Bearer ddA424aD2Da7aWs9D22A Content-Type: application/octet-stream charset=UTF-8"
             */
            method: 'POST',
            path: '/post',
            config: {
                auth: 'manager',
                payload: {
                    output: 'stream',
                    parse: true,
                    //allow: 'multipart/form-data',
                },
                handler: function (request, reply) {
                    
                    var data = request.payload;
                    if (data.file) {
                        var name = randtoken.generate(20, config.utils.randomID) + ".jpg";
                        var path = __dirname + "/assets/images/" + name;
                        var file = fs.createWriteStream(path);
                        file.on('error', function (err) { 
                            return reply(Boom.badData); 
                        });

                        data.file.pipe(file);
                        data.file.on('end', function (err) { 
                            if(err) {
                                return reply(Boom.badData); 
                            }
                            else {
                                var rspInfo = {
                                    statusCode: 200,
                                    error: null,
                                    message: 'success',
                                    fileName : name
                                }
                                return reply(rspInfo)
                            }
                        });
                    }
                    else {
                        return reply(Boom.badData('File not provided'));
                    }
                }
            }
        },
        {
            method: 'GET',
            path: '/assets/image/{fileID}', 
            config: {
                handler: function(request, reply) {
                    var path = __dirname + "/assets/images/" + request.params.fileID;
                    if(!fs.existsSync(path)) {
                        return reply(Boom.badData('file does not exist'));
                    }
                    else {
                        var options = {
                            confine: false
                        }
                        return reply.file(path, options).vary('x-magic');
                    }
                }
            }
        },
        {
            method: 'PATCH',
            path: '/remove/{fileID}',
            config: {
                auth: 'manager',
                handler: function(request, reply) {
                    var path = __dirname + "/assets/images/" + request.params.fileID;
                    if(fs.existsSync(path)) {
                        fs.unlinkSync(path);
                        var rspInfo = {
                            statusCode: 200,
                            error: null,
                            message: 'success'
                        }
                        return reply(rspInfo)
                    }
                    else {
                        return reply(Boom.badData('file does not exist'));
                    }
                }
            }
        }
    ]);

});

server.register(Inert, () => {});

server.start();
console.log('Ekumbu Media listening at port ' + server.info.port);

